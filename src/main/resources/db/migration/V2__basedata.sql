insert into character (name, race, age)
values('Bilbo', 'Hobbit', 50);

insert into character (name, race, age)
values('Frodo', 'Hobbit', 21);

insert into character (name, race, age)
values('Gandalf', 'Wizard', 9999);

insert into character (name, race, age)
values('Aragorn', 'Man', 210);

insert into character (name, race, age)
values('Boromir', 'Man', 41);

insert into character (name, race, age)
values('Gimli', 'Dwarf', 262);

insert into character (name, race, age)
values('Legolas', 'Elf', 2931);

insert into character (name, race, age)
values('Sauron', 'Evil', 9999);

insert into character (name, race, age)
values('Gollum', 'Hobbit', 600);