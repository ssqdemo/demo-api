CREATE TABLE CHARACTER (
	id BIGSERIAL PRIMARY KEY,
	name varchar(255) not null,
	race varchar(255) not null,
	age integer not null
);

