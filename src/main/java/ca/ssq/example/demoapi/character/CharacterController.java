package ca.ssq.example.demoapi.character;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin
@RestController
public class CharacterController {

	@Autowired
	private CharacterRepository characterRepository;


	@GetMapping("/characters")
	public Page<Character> retrieveAllCharacters(Pageable pageable) {
		return characterRepository.findAll(pageable);
	}	
	
	
	@GetMapping("/characters/{id}")
	public Character retrieveStudent(@PathVariable long id) {
		Optional<Character> character = characterRepository.findById(id);

		if (!character.isPresent())
			throw new CharacterNotFoundException("id-" + id);

		return character.get();
	}	
	
	@DeleteMapping("/characters/{id}")
	public void deleteCharacter(@PathVariable long id) {
		characterRepository.deleteById(id);
	}	
	
	
	@PostMapping("/characters")
	public ResponseEntity<Object> createCharacter(@RequestBody Character character) {
		Character savedCharacter = characterRepository.save(character);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedCharacter.getId()).toUri();

		return ResponseEntity.created(location).build();

	}	
	
	
	@PutMapping("/characters/{id}")
	public ResponseEntity<Object> updateStudent(@RequestBody Character character, @PathVariable long id) {

		Optional<Character> characterOptional = characterRepository.findById(id);

		if (!characterOptional.isPresent())
			return ResponseEntity.notFound().build();

		character.setId(id);
		
		characterRepository.save(character);

		return ResponseEntity.noContent().build();
	}	
		
	
	
}
